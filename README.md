# ${widget.name}
${widget.description}

# Information
| Name       |  ${widget.name}  |
|------------|------------------|
| Version    | ${widget.version}|
| Bundle     | ${widget.bundle} |
| Author     | ${widget.author} |
| Icon       | ![icon](icon.png)|
| Status     | [![Build Status](${widget.status}/badge/icon)](${widget.status}) |

## Widget Checklist

 - [ ] Fault Tolerance: Widget gracefully behaves/fails with loss of connection.
 - [ ] Fault Tolerance: Widget gracefully fails if session is lost.
 - [ ] Fault Tolerance: Widget gracefully and productively handles error responses.
 - [ ] Extensibility: Look and feel is manageable via theming.
 - [ ] Extensibility: Decomposable.
 - [ ] Security: Secure from XSS.
 - [ ] Security: Secure from CSRF.
 - [ ] Accessibility: Support for color blind users.
 - [ ] Accessibility: Support for users with motor-inability (keyboard navigation).
 - [ ] Accessibility: Support for users who are blind (screen reader).
 - [ ] i18n: All UI messages are externalized and localizable.
 - [ ] i18n: All dates and numbers are localized.
 - [ ] i18n: Works RTL.
 - [ ] Mobile: SDK compatible.
 - [ ] Mobile: Widget is responsive to mobile & tablet.
 - [ ] Documentation: Reference files linked from README.
 - [ ] Documentation: Dependencies (bower & UI components used) listed in README.
 - [ ] Documentation: Modules/classes JSDoc.
 - [ ] Documentation: Widget feature list documented.
 - [ ] Testing: Distribution folder.
 - [ ] Testing: Unit testing.
 - [ ] Testing: Functional testing.

## Usage
Install library

```bash
bower install ${widget.name} -S
```

## Dependencies

## Preferences

## Events

## Custom Components

## Requirements

## References

## Development

### Development Dependencies


```bash
git clone <repo-url> && cd <widget-path>
bower install
```

### Standalone

```bash
bblp start -e
```

### Test
- watch and add coverage

```bash
bblp test -wc
```


### Build

```bash
bblp docs && bblp build -t
```


### Deploy

-to default running local portal

```bash
bblp deploy
```

